from django.db import models
from django.contrib.auth.models import User
from myshop.product.models import Product


class Cart(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    count = models.IntegerField(default=1)

    def get_item_price(self):
        return self.product.price*self.count

