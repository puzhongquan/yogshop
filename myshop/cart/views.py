from django.shortcuts import render, HttpResponse, redirect
from myshop.cart.models import *


def add_cart(request):
    product_id=request.POST.get('product_id')
    if request.user:
        Cart.objects.get_or_create(user=request.user, product_id=product_id)

def add_count(request):
    if request.POST.get('cart_id'):
        cart = Cart.objects.filter(id=request.POST.get('cart_id')).first()
        cart.count += 1
        cart.save()
        return HttpResponse('yes')

def detail(request):
    carts = Cart.objects.filter(user=request.user)
    price = 0
    for cart in carts:
        price = price + cart.get_item_price()
    return render(request, 'cart/cart-detail.html',{
        'carts': carts,
        'total_price': price,
    })