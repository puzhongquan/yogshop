from django.conf.urls import url
from myshop.cart import views

urlpatterns = [
    url(r'^add_count/$', views.add_count, name='add-count'),
    url(r'^cart_detail/$', views.detail, name='cart-detail'),
    url(r'^add_cart/$', views.add_cart, name='add-cart'),

]
