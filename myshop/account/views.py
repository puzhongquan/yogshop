from django.shortcuts import render, HttpResponse, redirect
from myshop.account.models import UserFavorite
from myshop.product.models import Product
from myshop.account.forms import UserForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as login_user
from django.contrib.auth import logout as logout_user


def add_fav(request):
    user = request.user
    fav_id = request.POST.get('fav_id', 0)
    if fav_id:
        product = Product.objects.filter(id=fav_id)
        if UserFavorite.objects.filter(user=user, fav_id=fav_id).exists():
            UserFavorite.objects.filter(user=user, fav_id=fav_id).delete()
            return HttpResponse('取消收藏')
        else:
            UserFavorite.objects.create(user=user, fav_id=fav_id)
            return HttpResponse('已收藏')


def login(request):
    form = UserForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
    user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
    if user:
        login_user(request, user)
        return redirect('/')

    return render(request, 'account/login.html', {
        'form': form,
    })

def logout(request):
    if request.user:
        logout_user(request)
        return redirect('/')

def fav_list(request):
    ufs = UserFavorite.objects.filter(user=request.user)
    uf_list = []
    for uf in ufs:
        uf_list.append(uf.fav_id)
    products = Product.objects.filter(id__in=uf_list)

    return render(request, 'account/fav_list.html', {
        'products': products,
    })