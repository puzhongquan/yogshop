from django.db import models
from django.contrib.auth.models import User

# class UserProfile(models.Model):
#     user = models.OneToOneField(User)


class UserFavorite(models.Model):
    user = models.ForeignKey(User)
    fav_id = models.IntegerField(default=0)
