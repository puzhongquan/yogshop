from django.conf.urls import url


from myshop.account import views

urlpatterns = [
    url(r'^add_fav/$', views.add_fav, name='add-fav'),
    url(r'^fav_list/$', views.fav_list, name='fav-list'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^login/$', views.login, name='login'),
]
