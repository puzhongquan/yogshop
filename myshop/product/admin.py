from django.contrib import admin
from myshop.product.models import *


class ImageInline(admin.StackedInline):
    model = ProductImage
    show_change_link = True


class ProductAdmin(admin.ModelAdmin):
    inlines = [ImageInline]
admin.site.register(Product, ProductAdmin)

admin.site.register(ProductImage)
admin.site.register(Category)
