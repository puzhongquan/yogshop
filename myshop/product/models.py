from __future__ import unicode_literals

from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=200)

    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField()
    description = models.TextField(blank=True,null=True)

    status = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_first_image(self):
        self.productimage_set.first()

class ProductImage(models.Model):
    product = models.ForeignKey(Product, null=True, blank=True)
    image = models.ImageField(upload_to='product/%Y/%m', max_length=255)
    display_order = models.PositiveSmallIntegerField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s : %s'%(self.product.name, self.display_order)