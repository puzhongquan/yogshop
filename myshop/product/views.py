from django.shortcuts import render, HttpResponse, get_object_or_404

from myshop.product.models import *
from myshop.account.models import *
from myshop.cart.models import *


def product_list(request):
    products = Product.objects.filter(status=True)
    carts = Cart.objects.filter(user=request.user)
    return render(request, 'product/list.html', {
        'products': products,
        'carts': carts,
    })


def product_detail(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    carts = Cart.objects.filter(user=request.user)
    fav = False
    if request.user.is_authenticated():
        fav = UserFavorite.objects.filter(user=request.user, fav_id=product_id).exists()
    if product:
        return render(request, 'product/detail.html', {
            'product': product,
            'images': product.productimage_set.all(),
            'fav': fav,
            'carts': carts,
        })
