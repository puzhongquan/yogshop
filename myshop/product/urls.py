from django.conf.urls import url


from myshop.product import views

urlpatterns = [
    url(r'^list/$', views.product_list, name='product-list'),
    url(r'^detail/(?P<product_id>\d+)/$', views.product_detail, name='product-detail'),
]
